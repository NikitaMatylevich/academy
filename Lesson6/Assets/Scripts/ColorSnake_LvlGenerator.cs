﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class ColorSnake_LvlGenerator : MonoBehaviour
{
    [SerializeField] private ColorSnake_Types m_Types;

    [SerializeField] private ColorSnake_GameController m_Controller;

    private int line = 1;

    private List<GameObject> obstacles = new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        var upBorder = m_Controller.Bounds.Up;

        while (line * 2f < upBorder + 2f)
        {
            GenerateObstacle();
        }
    }

    // Update is called once per frame
    void Update()
    {
        var upBorder = m_Controller.Bounds.Up + m_Controller.Camera.transform.position.y;
        if (line * 2f > upBorder + 2)
        {
            return;
            
        }
        
        GenerateObstacle();
        
        
    }

    private void GenerateObstacle()
    {
        var template = m_Types.GetRandomTemplateType();
        var obstacle = new GameObject($"obstacle_{line}");

        foreach (var point in template.Points)
        {
            var objType = m_Types.GetRandomObjectType();
            var colorType = m_Types.GetRandomColortype();
            
            var obj = Instantiate(objType.Object, point.position, point.rotation);
            obj.transform.parent = obstacle.transform;

            obj.GetComponent<SpriteRenderer>().color = colorType.Color;

            var obstacleComponent = obj.AddComponent<ColorSneak_Obstacles>();
            obstacleComponent.ColorId = colorType.Id;
        }

        Vector3 pos = obstacle.transform.position;
        pos.y = (line * 20);

        obstacle.transform.position = pos;

        line++;
        
        obstacles.Add(obstacle);
    }
}
