﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using Random = System.Random;

public class ColorSnake_Types : MonoBehaviour
{
    [Serializable]
    public class colorType
    {
        public string Name;
        public int Id;
        public Color Color;
    }
    
    [Serializable]
    public class objectType
    {
        public string Name;
        public int Id;
        public GameObject Object;
    }
    
    [Serializable]
    public class templatesType
    {
        public string Name;
        public int Id;
        public Transform[] Points;
    }

    [SerializeField] private colorType[] m_Colors;
    [SerializeField] private objectType[] m_Objects;
    [SerializeField] private templatesType[] m_Templates;

    public colorType GetRandomColortype()
    {
        int rand = UnityEngine.Random.Range(0, m_Colors.Length);
        return m_Colors[rand];
    }

    public objectType GetRandomObjectType()
    {
        int rand = UnityEngine.Random.Range(0, m_Objects.Length);
        return m_Objects[rand];
    }
    
    public templatesType GetRandomTemplateType()
    {
        int rand = UnityEngine.Random.Range(0, m_Templates.Length);
        return m_Templates[rand];
    }

    public colorType GetColorType(int id)
    {
        return m_Colors.FirstOrDefault(c => c.Id == id);
    }

    public objectType GetObjectType(int id)
    {
        return m_Objects.FirstOrDefault(c => c.Id == id);
    }
}
